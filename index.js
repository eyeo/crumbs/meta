module.exports = {
  domains: require('./domains.json'),
  ages: require('./ages.json'),
  genders: require('./genders.json'),
  languages: require('./languages.json'),
  interests: require('./interests.json'),
}
