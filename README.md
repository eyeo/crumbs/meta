# Crumbs Meta

Meta data for gathering insights by Crumbs.

For how to use this please check [crumbs-extension](https://gitlab.com/eyeo/crumbs/crumbs-extension).

List of languages taken from:
- https://raw.githubusercontent.com/meikidd/iso-639-1/master/src/data.js

Content classification from:
- https://cloud.google.com/natural-language/docs/reference/rest/v1/documents/classifyText
- https://cloud.google.com/natural-language/docs/categories

## License

Licensed under GPL 3.0;
you may not use this files except in compliance with the License.
You may obtain a copy of the License at https://www.gnu.org/licenses/gpl-3.0.en.html
